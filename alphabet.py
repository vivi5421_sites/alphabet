from flask import Flask
from flask_restplus import Resource, Api

app = Flask(__name__)
api = Api(app)

def alphabet(letter):
        return ord(letter.upper()) - ord("A") + 1

def alphabets(letters="ABCDEFGHIJKLMNOPQRSTUVWXYZ"):
        return {k: alphabet(k) for k in letters} 

@api.route("/")
class Alphabet(Resource):

        def get(self):
                """
                Return ranks for all letters.
                """
                return alphabets()

@api.route("/<string:letters>")
@api.doc(params={'letters': 'The letters you want to get the rank'})
class AlphabetLetters(Resource):

        def get(self, letters):
                """
                Return ranks for given letters.
                """
                return alphabets(letters)

if __name__ == '__main__':
    app.run(debug=True, host="0.0.0.0", port=7003)